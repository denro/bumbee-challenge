create table visits (
  device text,
  lat float,
  long float,
  time timestamp with time zone
)
