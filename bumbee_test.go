package main_test

import (
	. "bumbee"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	ghttp "github.com/onsi/gomega/ghttp"

	"github.com/gin-gonic/gin"

	"net/http"
	"net/http/httptest"

	"encoding/json"
	"fmt"
	"strings"

	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"time"
)

type fakeFacade struct {
	HaveBeenCalled bool
	Data           *TrackingData
}

func (f *fakeFacade) Handle(td *TrackingData) {
	f.HaveBeenCalled = true
	f.Data = td
}

var _ = Describe("TrackerHandler", func() {
	var (
		facade *fakeFacade

		router   *gin.Engine
		response *httptest.ResponseRecorder

		trackingDataJSON = `{
			"node_type":1,
			"visits":[
				{"lat":51.23, "long":25.543, "time":1458572383, "device":"727d2a99b62d5759", "rssi":-60},
				{"lat":51.231, "long":25.5, "time":1458572381, "device":"727d2a99b62d5759", "rssi":-60},
				{"lat":51.23, "long":25.54, "time":1458572383, "device":"727d2a99b62d5759", "rssi":-80}
			]
		}`
	)

	BeforeEach(func() {
		facade = &fakeFacade{}

		gin.SetMode(gin.TestMode)
		router = gin.New()
		router.POST("/foo", TrackerHandler(facade))

		response = httptest.NewRecorder()
	})

	Context("When given valid JSON data", func() {
		It("returns a successful JSON response", func() {
			request, _ := http.NewRequest("POST", "/foo", strings.NewReader(trackingDataJSON))

			router.ServeHTTP(response, request)

			Expect(response.Code).To(Equal(200))
			Expect(response.Body.String()).To(BeEquivalentTo(`{"status":"ok"}` + "\n"))
		})

		It("sends the tracker data to the handler", func() {
			request, _ := http.NewRequest("POST", "/foo", strings.NewReader(trackingDataJSON))

			router.ServeHTTP(response, request)

			trackingData := func(rawJSON string) *TrackingData {
				td := &TrackingData{}
				json.NewDecoder(strings.NewReader(rawJSON)).Decode(td)
				return td
			}(trackingDataJSON)

			Expect(facade.HaveBeenCalled).To(BeTrue())
			Expect(facade.Data).To(BeEquivalentTo(trackingData))
		})
	})

	It("returns a erroneous JSON response given invalid JSON data", func() {
		request, _ := http.NewRequest("POST", "/foo", strings.NewReader(`{"node_type":1}`))

		router.ServeHTTP(response, request)

		Expect(response.Code).To(Equal(400))
		Expect(response.Body.String()).To(BeEquivalentTo(`{"status":"validation_error"}` + "\n"))
	})
})

var _ = Describe("VisitList", func() {
	Describe("Cleanup", func() {
		It("returns a list with removed duplicated entries", func() {
			visits := VisitList{
				{Lat: 1, Long: 1, RSSI: 1, Time: 2, Device: "test"},
				{Lat: 1, Long: 1, RSSI: 2, Time: 2, Device: "test"},
			}

			visits = visits.Cleanup()

			expectedList := VisitList{
				{Lat: 1, Long: 1, RSSI: 2, Time: 2, Device: "test"},
			}

			Expect(visits).To(BeEquivalentTo(expectedList))
		})

		It("returns entries that are not duplicated", func() {
			visits := VisitList{{Device: "test2"}}

			visits = visits.Cleanup()

			Expect(visits).To(BeEquivalentTo(visits))
		})
	})
})

var _ = Describe("ArchiveFormat", func() {
	Describe("NewArchiveFormat", func() {
		It("initializes a new archive format", func() {
			visits := VisitList{
				{Lat: 1, Long: 1, RSSI: 1, Time: 1},
				{Lat: 2, Long: 2, RSSI: 2, Time: 2},
			}

			expectedArchiveFormat := &ArchiveFormat{
				NodeType:     "node_type",
				Device:       "device",
				TimeStart:    1,
				TimeEnd:      2,
				RSSIMin:      1,
				RSSIMax:      2,
				SamplesCount: 2,
				AveragePosition: AvgPos{
					Latitute:  1.5,
					Longitude: 1.5,
				},
			}

			af := NewArchiveFormat("node_type", "device", visits)
			Expect(af).To(BeEquivalentTo(expectedArchiveFormat))
		})
	})

})

var _ = Describe("ServieAdapter", func() {
	var (
		sendFuncWasCalled bool
	)

	BeforeEach(func() {
		sendFuncWasCalled = false
	})

	Describe("Archive", func() {
		It("calls the archive formatter with tracking data", func() {
			visits := VisitList{{Device: "test"}}

			archiveFormat := &ArchiveFormat{
				NodeType:     "tracker",
				Device:       "test",
				SamplesCount: 1,
			}

			fakeSendFunc := func(af *ArchiveFormat, url string) {
				sendFuncWasCalled = true

				Expect(af).To(BeEquivalentTo(archiveFormat))
				Expect(url).To(BeEquivalentTo("FAKEURL"))
			}

			adapter := &ServiceAdapter{
				ArchiveURL: "FAKEURL",
				SendFunc:   fakeSendFunc,
			}

			adapter.Archive(1, visits)

			Expect(sendFuncWasCalled).To(Equal(true))
		})

		It("does nothing when given tracking data from invalid nodes", func() {
			adapter := &ServiceAdapter{}
			adapter.Archive(3, VisitList{})

			Expect(sendFuncWasCalled).To(Equal(false))

			adapter.Archive(4, VisitList{})

			Expect(sendFuncWasCalled).To(Equal(false))
		})
	})
})

var _ = Describe("DatabaseAdapter", func() {
	Describe("Store", func() {
		It("stores the given visits", func() {
			visits := VisitList{{Device: "test", Lat: 2.0, Long: 1.0, Time: 123}}

			db, mock, err := sqlmock.New()
			Expect(err).ToNot(HaveOccurred())
			defer db.Close()

			mock.ExpectExec("INSERT INTO visits").WithArgs("test", 2.0, 1.0, time.Unix(123, 0))

			adapter := &DatabaseAdapter{DB: db}
			adapter.Store(visits)

			Expect(mock.ExpectationsWereMet()).ToNot(HaveOccurred())
		})
	})
})

var _ = Describe("JSONSend", func() {
	It("sends the given archive fromat as json to given url", func() {
		fakeServer := ghttp.NewServer()

		af := &ArchiveFormat{
			NodeType: "test",
		}

		encodedJSON, _ := json.Marshal(af)
		fakeServer.AppendHandlers(ghttp.CombineHandlers(
			ghttp.VerifyRequest("POST", "/archive"),
			ghttp.VerifyBody(append(encodedJSON, byte('\n'))),
		))

		JSONSend(af, fmt.Sprint(fakeServer.URL(), "/archive"))

		fakeServer.Close()
	})
})
