package main

import (
	"fmt"
)

type Visit struct {
	Lat    float64 `json:"lat" binding:"required"`
	Long   float64 `json:"long" binding:"required"`
	Time   int64   `json:"time" binding:"required"`
	Device string  `json:"device" binding:"required"`
	RSSI   int64   `json:"rssi" binding:"required"`
}

type VisitList []Visit

func (vl VisitList) Cleanup() VisitList {
	clean := VisitList{}
	tempMap := map[string]Visit{}

	for _, v := range vl {
		key := fmt.Sprintf("%v:%v:%v:%v", v.Device, v.Time, v.Long, v.Lat)
		if curr, exist := tempMap[key]; exist && v.RSSI > curr.RSSI {
			tempMap[key] = v
		} else if !exist {
			tempMap[key] = v
		}
	}

	for _, v := range tempMap {
		clean = append(clean, v)
	}

	return clean
}

type TrackingData struct {
	NodeType int       `json:"node_type" binding:"required"`
	Visits   VisitList `json:"visits" binding:"required"`
}
