package main_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestBumbee(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Bumbee Suite")
}
