package main

import (
	"database/sql"
	"github.com/gin-gonic/gin"
	_ "github.com/joho/godotenv/autoload"
	_ "github.com/lib/pq"
	"log"
	"net/http"
	"os"
)

func TrackerHandler(h TrackerDataHandler) gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data TrackingData
			err  error
		)

		if err = c.BindJSON(&data); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"status": "validation_error"})
			return
		}

		h.Handle(&data)

		c.JSON(http.StatusOK, gin.H{"status": "ok"})
	}
}

func AuthRequired(c *gin.Context) {
	token := c.Request.Header.Get("X-Auth-Token")

	if token != os.Getenv("API_TOKEN") {
		c.JSON(http.StatusForbidden, gin.H{"status": "forbidden"})
		c.Abort()
	}

	c.Next()
}

func ViewerHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "viewer.tmpl", nil)
}

func DataHandler(da *DatabaseAdapter) gin.HandlerFunc {
	return func(c *gin.Context) {
		vl, err := da.Latest(100)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"status": "internal_server_error"})
			log.Println(err)
			return
		}

		c.JSON(http.StatusOK, vl)
	}
}

func main() {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	da := &DatabaseAdapter{DB: db}
	sa := &ServiceAdapter{
		ArchiveURL: os.Getenv("ARCHIVE_URL"),
		SendFunc:   JSONSend,
	}

	facade := &TrackerFacade{
		StorageAdapter: da,
		ArchiveAdapter: sa,
	}

	router := gin.Default()

	router.LoadHTMLFiles("views/viewer.tmpl")
	router.Static("/assets", "./views/assets")

	router.GET("/viewer", ViewerHandler)
	router.GET("/data", DataHandler(da))

	router.POST("/api/tracker", AuthRequired, TrackerHandler(facade))

	router.Run() // listen and serve on 0.0.0.0:8080
}
