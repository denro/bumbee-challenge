package main

type AvgPos struct {
	Latitute  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

type ArchiveFormat struct {
	NodeType        string `json:"node_type"`
	Device          string `json:"device"`
	TimeStart       int64  `json:"time_start"`
	TimeEnd         int64  `json:"time_end"`
	RSSIMin         int64  `json:"rssi_min"`
	RSSIMax         int64  `json:"rssi_max"`
	SamplesCount    int64  `json:"samples_count"`
	AveragePosition AvgPos `json:"average_position"`
}

func NewArchiveFormat(nodeType, device string, visits VisitList) *ArchiveFormat {
	var timeList, rssiList []int64
	var totalLat, totalLong float64

	// Generate archive format
	for _, v := range visits {
		timeList = append(timeList, v.Time)
		rssiList = append(rssiList, v.RSSI)
		totalLat = totalLat + v.Lat
		totalLong = totalLong + v.Long
	}

	minTime, maxTime := minmax(timeList)
	minRSSI, maxRSSI := minmax(rssiList)

	return &ArchiveFormat{
		NodeType:     nodeType,
		Device:       device,
		TimeStart:    minTime,
		TimeEnd:      maxTime,
		RSSIMin:      minRSSI,
		RSSIMax:      maxRSSI,
		SamplesCount: int64(len(visits)),
		AveragePosition: AvgPos{
			Latitute:  totalLat / float64(len(visits)),
			Longitude: totalLong / float64(len(visits)),
		},
	}
}

func minmax(list []int64) (int64, int64) {
	min, max := list[0], list[0]
	for _, i := range list {
		if min > i {
			min = i
		}

		if max < i {
			max = i
		}
	}

	return min, max
}
