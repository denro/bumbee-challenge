package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	_ "github.com/lib/pq"
	"net/http"
	"time"
)

type TrackerDataHandler interface {
	Handle(*TrackingData)
}

type Storer interface {
	Store(VisitList) error
}

type Archiver interface {
	Archive(int, VisitList)
}

type TrackerFacade struct {
	StorageAdapter Storer
	ArchiveAdapter Archiver
}

func (f *TrackerFacade) Handle(td *TrackingData) {
	td.Visits = td.Visits.Cleanup()
	f.StorageAdapter.Store(td.Visits)
	f.ArchiveAdapter.Archive(td.NodeType, td.Visits)
}

type DatabaseAdapter struct {
	DB *sql.DB
}

var sqlInsert = "INSERT INTO visits (device, lat, long, time) VALUES ($1,$2,$3,$4)"

func (da *DatabaseAdapter) Store(vl VisitList) error {
	for _, v := range vl {
		if _, err := da.DB.Exec(sqlInsert, v.Device, v.Lat, v.Long, time.Unix(v.Time, 0)); err != nil {
			return err
		}
	}

	return nil
}

var sqlSelect = "SELECT device, lat, long, time FROM visits ORDER BY time DESC LIMIT $1"

func (da *DatabaseAdapter) Latest(count int64) (VisitList, error) {
	rows, err := da.DB.Query(sqlSelect, count)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	visits := VisitList{}
	for rows.Next() {
		var v Visit
		var t time.Time

		if err := rows.Scan(&v.Device, &v.Lat, &v.Long, &t); err != nil {
			return nil, err
		}

		v.Time = t.Unix()

		visits = append(visits, v)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return visits, nil
}

type ServiceAdapter struct {
	SendFunc   func(*ArchiveFormat, string)
	ArchiveURL string
}

func (s *ServiceAdapter) Archive(nodeType int, vl VisitList) {
	if nodeType >= 2 {
		return
	}

	tmpMap := map[string]VisitList{}
	for _, visit := range vl {
		if list, exist := tmpMap[visit.Device]; exist {
			tmpMap[visit.Device] = append(list, visit)
		} else {
			tmpMap[visit.Device] = VisitList{visit}
		}
	}

	for device, visits := range tmpMap {
		s.SendFunc(NewArchiveFormat(strNodeTypes[nodeType], device, visits), s.ArchiveURL)
	}
}

func JSONSend(af *ArchiveFormat, url string) {
	var jsonBody bytes.Buffer
	json.NewEncoder(&jsonBody).Encode(af)

	http.Post(url, "application/json", &jsonBody)
}

var strNodeTypes = map[int]string{
	1: "tracker",
	2: "internet_provider",
	3: "mobile_station",
	4: "drone",
}
