# Bumbee
A service for tracking and viewing recorded devices geographically.
Interaction with the API Endpoints is in JSON.

### Descicions and Limitations
Go was decided as the language of choice mostly for the fun of it. There are a few small frameworks for building web applications and i wanted to try out Gin. It offered an easy to use syntax, good features, while keeping performance. Other than that I've tried to limit the amount of external libraries in order to try out the std-lib, most packages used are for testing purposes.

TDD was applied as much as it was possible, in the end i ran out of time and had to spike out most of the code in order to finsh.

Design-wise i choose a facade pattern for the storing the tracking data, it takes a `Storer` adapter and an `Archiver` adapter, which are responsible for storing and archiving without having to clobber up and also making it a lot easier to test.

Futher limitations is that the `ServiceSorage` doesn't have an internal que for its messages to the external archive service, making downtime or connection problems fatal.

Looking back i would probably have done this in a more familiar language/framework since it took quite a lot of time, but trying out different frameworks and tools was an additional goal of mine.

### Dependencies
* *nix (developed on OSX, unsure if windows works)
* Go (any recent version should work)
* Postgres (any recent version should work)

### Installation and Usage
All settings are configurable in the .env-file.

#### Setting up the database
```sh
$ createdb trioptima
$ psql -d myDataBase -a -f db/schema.sql
```

#### Running the server
```sh
$ go build
$ ./bumbee
```


